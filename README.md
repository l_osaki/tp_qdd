# TP_QDD

ETAPE 1 :

- [X] Afficher sur un fond de carte les arrêts de bus de la ville de Nantes. utiliser une symbolique
- [X] afficher sur le même fond de carte les arrêts de TRAM de la ville de Nantes.
- [ ] proposer l'affichage (click droit ou passage sur le point) des métadata sur les arrêts. Accès handicapé, correspondances Lignes, liens avec le Tram).
- [ ] relier graphiquement les arrêts avec un code couleur différents pour chaque ligne. 

ETAPE 2 :

- [ ] relier graphiquement les arrêts avec un code couleur différents pour chaque ligne en fonction des routes disponibles sur l'Open Data de la ville de Nantes.
- [ ] bonus mettre une photo de l'arrêt (à récupérer par exemple sur Google Street View) : à faire pour quelques arrêts uniquement.
- [ ] proposer un filtrage sur l'affichage des metada et sur ligne bus ou Tram
- [ ] proposer une légende explicite sur votre affichage.

