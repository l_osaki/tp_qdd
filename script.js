var map = L.map('map').setView([47.2184, -1.5536], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '© OpenStreetMap contributors'
}).addTo(map);

var highlightedCircuitLayer;

// Récupérer les données depuis le fichier JSON local (arrêts de bus)
fetch('tan-arrets.json')
    .then(response => response.json())
    .then(busStops => {
        // Afficher les marqueurs et la liste des arrêts de bus
        var busStopsList = document.getElementById('bus-stops-list');

        busStops.forEach(busStop => {
            var marker = L.marker([busStop.stop_coordinates.lat, busStop.stop_coordinates.lon], {
                icon: L.divIcon({
                    className: 'custom-marker',
                    html: '<div></div>',
                    iconSize: [3, 3],
                    iconAnchor: [1.5, 1.5],
                })
            }).addTo(map);

            marker.bindPopup("Nom de l'arrêt de bus : " + busStop.stop_name);

            // Ajouter à la liste
            var listItem = document.createElement('div');
            listItem.className = 'bus-stop-item';
            listItem.textContent = "Nom de l'arrêt de bus : " + busStop.stop_name;
            busStopsList.appendChild(listItem);

            // Ajouter un événement de clic pour chaque marqueur
            marker.on('click', function () {
                var circuitId = busStop.circuit_id;
                highlightCircuit(circuitId);
            });
        });
    })
    .catch(error => console.error('Erreur lors de la récupération des données (arrêts de bus) :', error));

// Récupérer les données depuis le fichier JSON local (circuits)
fetch('tan-circuits.json')
    .then(response => response.json())
    .then(circuits => {
        console.log('Données des circuits :', circuits);
    })
    .catch(error => console.error('Erreur lors de la récupération des données (circuits) :', error));

// Fonction pour mettre en surbrillance le circuit NE MARCHE PAS
function highlightCircuit(circuitId) {
    var circuit = findCircuitById(circuits, circuitId);

    if (circuit) {
        if (highlightedCircuitLayer) {
            map.removeLayer(highlightedCircuitLayer);
        }

        highlightedCircuitLayer = L.geoJSON(circuit, {
            style: {
                color: 'red',
                weight: 2
            }
        }).addTo(map);
    }
}

// Fonction pour trouver un circuit par ID
function findCircuitById(circuits, circuitId) {
    return circuits.find(circuit => circuit.id === circuitId);
}
